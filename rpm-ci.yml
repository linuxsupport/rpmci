---
variables:
  _KOJI_PROFILE: 'koji'
  ADD_VCS_TAG: 'True'
  SKIP_KOJI_DOWNLOAD: 'False'
  RPM_AUTOSPEC: 'False'
  _VCS_TAG: "${CI_PROJECT_URL}/-/tree/${CI_COMMIT_SHA}"
  IMAGE_KOJI: 'gitlab-registry.cern.ch/linuxsupport/rpmci/kojicli'
  IMAGE_9al: 'gitlab-registry.cern.ch/linuxsupport/rpmci/builder-al9'
  IMAGE_9el: 'gitlab-registry.cern.ch/linuxsupport/rpmci/builder-el9'
  IMAGE_9: 'gitlab-registry.cern.ch/linuxsupport/rpmci/builder-cs9'
  IMAGE_8al: 'gitlab-registry.cern.ch/linuxsupport/rpmci/builder-al8'
  IMAGE_8el: 'gitlab-registry.cern.ch/linuxsupport/rpmci/builder-el8'
  IMAGE_8s: 'gitlab-registry.cern.ch/linuxsupport/rpmci/builder-cs8'
  IMAGE_8: 'gitlab-registry.cern.ch/linuxsupport/rpmci/builder-c8'
  IMAGE_7: 'gitlab-registry.cern.ch/linuxsupport/rpmci/builder-cc7'
  IMAGE_6: 'gitlab-registry.cern.ch/linuxsupport/rpmci/builder-slc6'
  BUILD_9al: 'False'
  BUILD_9el: 'False'
  BUILD_9: 'False'
  BUILD_8al: 'False'
  BUILD_8el: 'False'
  BUILD_8s: 'False'
  BUILD_8: 'False'
  BUILD_7: 'False'
  BUILD_6: 'False'
  DIST_9al: '.al9.cern'
  DIST_9el: '.rh9.cern'
  DIST_9: '.el9'
  DIST_8al: '.al8.cern'
  DIST_8el: '.rh8.cern'
  DIST_8s: '.el8s'
  DIST_8: '.el8'
  DIST_7: '.el7'
  DIST_6: '.slc6'
  KOJI_TAG_9al: ''
  KOJI_TAG_9el: ''
  KOJI_TAG_9: ''
  KOJI_TAG_8al: ''
  KOJI_TAG_8el: ''
  KOJI_TAG_8s: ''
  KOJI_TAG_8: ''
  KOJI_TAG_7: ''
  KOJI_TAG_6: ''
  AUTOTAG_QA: 'False'
  AUTOTAG_STABLE: 'False'

include:
  - 'https://gitlab.cern.ch/linuxsupport/rpmci/raw/master/debian.yml'
  - 'https://gitlab.cern.ch/linuxsupport/rpmci/raw/master/dist_9al.yml'
  - 'https://gitlab.cern.ch/linuxsupport/rpmci/raw/master/dist_9el.yml'
  - 'https://gitlab.cern.ch/linuxsupport/rpmci/raw/master/dist_9.yml'
  - 'https://gitlab.cern.ch/linuxsupport/rpmci/raw/master/dist_8al.yml'
  - 'https://gitlab.cern.ch/linuxsupport/rpmci/raw/master/dist_8el.yml'
  - 'https://gitlab.cern.ch/linuxsupport/rpmci/raw/master/dist_8s.yml'
  - 'https://gitlab.cern.ch/linuxsupport/rpmci/raw/master/dist_8.yml'
  - 'https://gitlab.cern.ch/linuxsupport/rpmci/raw/master/dist_7.yml'
  - 'https://gitlab.cern.ch/linuxsupport/rpmci/raw/master/dist_6.yml'

stages:
  - prebuild
  - srpm
  - rpm
  - lint
  - prekoji
  - koji_scratch
  - pretest
  - test
  - posttest
  - koji_build
  - postkoji
  - docker_prepare
  - docker_build
  - docker_test
  - deploy_qa
  - postqa
  - deploy_stable
  - postdeploy

.add_vcs_tag: &add_vcs_tag
  - |
    if [[ "$ADD_VCS_TAG" == 'True' && $_KOJI_OS != 6 ]]; then
      for SPEC in *.spec; do
        if ! grep -q "VCS:" "$SPEC"; then
          echo_info "Adding VCS tag to spec file: ${_VCS_TAG}"
          sed -i "1iVCS: ${_VCS_TAG}" "$SPEC"
        fi
      done
    fi

.rpm_deps:
  image: $IMAGE
  interruptible: true
  before_script:
    - rpmci_install_builddeps

.build_srpm:
  extends: .rpm_deps
  stage: srpm
  script:
    - |
      if [[ ${DIST::1} != '.' ]]; then
        echo_error "The DIST_* variable is malformed, it should always start with a dot (e.g., '.al9.cern'). The current value is: '${DIST}'."
        sleep 15
        exit 1
      fi
    - *add_vcs_tag
    - export MAKEFILE=`[ -e Makefile.koji ] && echo "Makefile.koji" || echo "Makefile"`
    - make -f $MAKEFILE srpm
    - |
      if [[ ! -d build/SRPMS/ ]]; then
        echo_error "build/SRPMS/ not found in current directory, please check your Makefile and your .spec to make sure your _topdir is correct"
        sleep 15
        exit 1
      fi
    - COUNT=`find build/SRPMS/ -name "*${DIST}.*src.rpm" | wc -l`
    - |
      if [[ $COUNT -eq 0 ]]; then
        echo_error "No source RPMs found that match build/SRPMS/*${DIST}.*src.rpm, please check your Makefile and your .spec"
        echo_red "More info: https://gitlab.cern.ch/linuxsupport/rpmci/-/blob/master/README.md#error-no-source-rpms-found"
        echo "Current directory is: `pwd`"
        ls -lR build/SRPMS/
        sleep 15
        exit 1
      fi
  artifacts:
    expose_as: 'Source RPM'
    paths:
      - build/SRPMS/
    expire_in: 1 month

.build_rpm:
  extends: .rpm_deps
  stage: rpm
  script:
    - *add_vcs_tag
    - export MAKEFILE=`[ -e Makefile.koji ] && echo "Makefile.koji" || echo "Makefile"`
    - make -f $MAKEFILE rpm
    - |
      if [[ ! -d build/RPMS/ ]]; then
        echo_error "build/RPMS/ not found, please check your Makefile and your .spec"
        echo "Current directory is: `pwd`"
        ls -lR build/RPMS/
        sleep 15
        exit 1
      fi
  dependencies: []
  artifacts:
    paths:
      - build/RPMS/
    expire_in: 1 month

.test_rpmlint:
  image: $IMAGE
  stage: lint
  interruptible: true
  script:
    - |
      if [[ -e .rpmlint ]]; then
        echo_info "Using custom rpmlint configuration from .rpmlint"
        CONFIG="-f .rpmlint"
      else
        echo_info "Using default rpmlint configuration. Create a .rpmlint file to override settings"
      fi
    - rpmlint -i $CONFIG build/

.test_install:
  image: $IMAGE
  stage: test
  interruptible: true
  before_script:
    - rpmci_install_koji_rpms
  script:
    - echo "Test me!"

.koji_deps:
  image: $IMAGE_KOJI
  interruptible: true
  before_script:
    - |
      if [[ -z "$KOJICI_USER" ]]; then
        echo_error "Variable KOJICI_USER not defined"
        sleep 15
        exit 1
      fi
    - |
      if [[ -z "$KOJICI_PWD" ]]; then
        echo_error "Variable KOJICI_PWD not defined"
        sleep 15
        exit 1
      fi
    - kinit.sh
    - mkdir koji
    - export _KOJITAG=$(get_kojitag)
  dependencies: []
  variables:
    GIT_STRATEGY: none

.koji_scratch:
  extends: .koji_deps
  stage: koji_scratch
  script:
    - |
      for SRPM in build/SRPMS/*${DIST}.*src.rpm; do
        export BUILDNAME=$(rpm -qp --qf "%{n}-%{v}-%{r}\n" ${SRPM})
        echo_info "Build name of ${SRPM} is \"${BUILDNAME}\""
        echo_command "koji -p ${_KOJI_PROFILE} build --wait --scratch ${_KOJITAG} ${SRPM}"
        koji -p ${_KOJI_PROFILE} build --wait --scratch ${_KOJITAG} ${SRPM} | tee taskid
        export TASKID=$(sed -z -e 's/.*Created task:\ \([0-9]\+\).*/\1/g' taskid)
        if [[ "$SKIP_KOJI_DOWNLOAD" == "False" ]]; then
          cd koji
          echo_command "koji -p ${_KOJI_PROFILE} download-task --noprogress $TASKID"
          koji -p ${_KOJI_PROFILE} download-task --noprogress $TASKID
          cd ~-
          export LOCAL_VERSION=$(rpm -qp --qf "%{v}-%{r}\n" ${SRPM})
          export KOJI_VERSION=$(rpm -qp --qf "%{v}-%{r}\n" koji/${BUILDNAME%${DIST}}*.src.rpm)
          echo_info "Local version is \"${LOCAL_VERSION}\", Koji version is \"${KOJI_VERSION}\"."
          if [[ "$KOJI_VERSION" != "$LOCAL_VERSION" ]]; then
            echo_error "The version created locally does not match the one created by Koji."
            echo_error "Make sure you specify the DIST_* variable correctly in your CI configuration,"
            echo_error "and that it matches configuration of your Koji tags set by the admins."
            sleep 15
            exit 1
          fi
        fi
        koji_task_summary $TASKID
      done
  artifacts:
    expose_as: 'Koji scratch RPMs'
    paths:
      - koji/
    expire_in: 1 month

.koji_build:
  extends: .koji_deps
  stage: koji_build
  only:
    refs:
      - tags
  script:
    - |
      for SRPM in build/SRPMS/*${DIST}.*src.rpm; do
        export PKGNAME=$(rpm -qp --qf "%{n}\n" ${SRPM})
        PKGEXISTS=$(koji -p ${_KOJI_PROFILE} list-pkgs --quiet --noinherit --tag="${_KOJITAG}-testing" --package="${PKGNAME}" > /dev/null 2>&1; echo $?)
        if [ $PKGEXISTS -eq 1 ]; then
          echo_error "Package ${PKGNAME} has not been added to the tag ${_KOJITAG}."
          echo "You can add it by running something like this:"
          echo "  for i in testing qa stable; do koji add-pkg --owner=$KOJICI_USER ${_KOJITAG}-\$i ${PKGNAME}; done"
          sleep 15
          exit 1
        fi
        export BUILDNAME=$(rpm -qp --qf "%{n}-%{v}-%{r}\n" ${SRPM})
        echo_info "Build name of ${SRPM} is \"${BUILDNAME}\""
        BUILDEXISTS=$(koji -p ${_KOJI_PROFILE} list-builds --quiet --buildid="${BUILDNAME}" 2>&1 | grep -q COMPLETE; echo $?)
        if [ $BUILDEXISTS -eq 0 ]; then
          # Let's see if that build comes from the same commit, in which case we can adopt it
          mkdir -p /tmp/koji
          cd /tmp/koji
          koji -p ${_KOJI_PROFILE} download-build --noprogress --arch src ${BUILDNAME}
          cd ~-
          # This assumes the VCS tag was set, of course. If it isn't, then we have no way of knowing
          # if this build is the same as the one we're trying to build, so we'll just fail.
          BUILD_VCS=$(rpm -qp --qf "%{VCS}\n" /tmp/koji/${BUILDNAME}.*src.rpm)
          if [[ "${_VCS_TAG}" != "${BUILD_VCS}" ]]; then
            echo_error "Build ${BUILDNAME} already exists in Koji, please update the version number."
            sleep 15
            exit 1
          fi
          echo_info "Build ${BUILDNAME} already exists in Koji and it was built from this same commit, adopting it."
          rm -rf /tmp/koji
        fi
        if [ $BUILDEXISTS -ne 0 ]; then
          echo_command "koji -p ${_KOJI_PROFILE} build --wait ${_KOJITAG} ${SRPM}"
          koji -p ${_KOJI_PROFILE} build --wait ${_KOJITAG} ${SRPM} | tee taskid
          export TASKID=$(sed -z -e 's/.*Created task:\ \([0-9]\+\).*/\1/g' taskid)
        fi
        if [[ "$SKIP_KOJI_DOWNLOAD" == "False" ]]; then
          cd koji
          echo_command "koji -p ${_KOJI_PROFILE} download-build --noprogress --debuginfo ${BUILDNAME}"
          koji -p ${_KOJI_PROFILE} download-build --noprogress --debuginfo ${BUILDNAME}
          cd ~-
        fi
        if [ $BUILDEXISTS -ne 0 ]; then
          koji_task_summary $TASKID
        fi
      done
  artifacts:
    expose_as: 'Koji RPMs'
    paths:
      - koji/
    expire_in: 1 month

.koji_tag:
  extends: .koji_deps
  variables:
    FULLTAGNAME: ""
    TAGSUFFIX: testing
  script:
    - |
      if [[ -n "${FULLTAGNAME}" ]]; then
        export TAGNAME="${FULLTAGNAME}"
      else
        export TAGNAME="${_KOJITAG}-${TAGSUFFIX}"
      fi
      echo_info "Tagging packages to ${TAGNAME}."
    - |
      for SRPM in build/SRPMS/*${DIST}.*src.rpm; do
        export BUILDNAME=$(rpm -qp --qf "%{n}-%{v}-%{r}\n" ${SRPM})
        echo_info "Build name of ${SRPM} is \"${BUILDNAME}\"."
        echo_command "koji -p ${_KOJI_PROFILE} tag-build ${TAGNAME} ${BUILDNAME}"
        koji -p ${_KOJI_PROFILE} tag-build ${TAGNAME} ${BUILDNAME}
      done

.tag_qa:
  extends: .koji_tag
  stage: deploy_qa
  when: manual
  rules:
    # Only for tags
    - if: '$CI_COMMIT_TAG != null'
  variables:
    TAGSUFFIX: qa
  allow_failure: false

.tag_stable:
  extends: .koji_tag
  stage: deploy_stable
  when: manual
  rules:
    # Only for tags
    - if: '$CI_COMMIT_TAG != null'
  variables:
    TAGSUFFIX: stable
