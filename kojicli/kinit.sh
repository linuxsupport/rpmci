
# Do a kinit if we have the ENV variables set
if [[ ! -z "${KOJICI_USER}" && ! -z "${KOJICI_PWD}" ]]; then
    echo_info "Getting Kerberos token for ${KOJICI_USER}:"
    echo "${KOJICI_PWD}" | kinit ${KOJICI_USER}@CERN.CH
    RET=$?
    if [[ $RET -ne 0 ]]; then
        echo_error "Error while running kinit"
        exit $RET
    fi
fi
