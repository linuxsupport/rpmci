# CI integration for building RPMs

This repo exists to help you build RPMs using Koji and Gitlab's CI infrastructure. You have here an [example of how to use it](https://gitlab.cern.ch/linuxsupport/myrpm).


## Prerequirements:

1. A [Koji tag](https://cern.service-now.com/service-portal?id=sc_cat_item&name=Koji-tag&fe=IT-RPM) where your packages will end up.
2. A [service account](https://account.cern.ch/account/Management/NewAccount.aspx) with [access to Koji](https://cern.service-now.com/service-portal?id=sc_cat_item&name=request&fe=IT-RPM), and in particular to your Koji tag. We'll use the account `kojici` as an example, but you must have your own. This account can be used for as many RPMs as you want.
3. A [Gitlab](https://gitlab.cern.ch/) repository with Pipelines (ie. continuous integration) enabled. You can enable Pipelines by going to *General Settings* for your repo and toggling the *Pipelines* feature under *Visibility, project features, permissions*. More info in [KB0003690](https://cern.service-now.com/service-portal?id=kb_article&n=KB0003690).
4. Source code with a `Makefile` (or a `Makefile.koji`, which will take precedence if both exist) that defines two mandatory targets, `srpm` and `rpm`. Those targets should build at least one source RPM and an RPM, respectively. Check our [RPM starter](https://gitlab.cern.ch/linuxsupport/myrpm) repo for a working example.

Note: RPMCI uses [interruptible](https://docs.gitlab.com/ee/ci/yaml/#interruptible) jobs in order to stop the execution of a pipeline if a newer pipeline has made it redundant. In order to take advantage of this feature, [Auto-cancel redundant pipelines](https://docs.gitlab.com/ee/ci/pipelines/settings.html#auto-cancel-redundant-pipelines) has to be enabled for your project.

## Procedure:

1. Create your new empty repo. *Protip: Create a subgroup for all the RPMs of your service/group, and create your new repo inside it.*
2. Copy **all** the files from our [RPM starter](https://gitlab.cern.ch/linuxsupport/myrpm) repo.
3. Edit the variables in `.gitlab-ci.yml` (see below) and fill in the name of your service account, your Koji tag, etc. **Don't modify the include!**
4. In Gitlab, go to *Settings > CI / CD > Variables*. Add two new variables called `KOJICI_USER` and `KOJICI_PWD` and set them to the username and password of your service account. `KOJICI_PWD` should be set as a [masked variable](https://gitlab.cern.ch/help/ci/variables/index.md#mask-a-cicd-variable) as an extra precaution but do note that it must comply with some requirements. *Protip: If you followed the protip in #1, you can set these variables at the subgroup level and all repos within it will be able to use them automatically!*
5. From *lxplus* or *aiadm*, manually add your package to your Koji tag. For example:
    ```bash
    ~ > koji add-pkg --owner=$USER mytag9al-testing myrpm
    ~ > koji add-pkg --owner=$USER mytag9al-qa myrpm
    ~ > koji add-pkg --owner=$USER mytag9al-stable myrpm
    ```
   Note that you **don't need to add your package to your `-build` tag**, and it may in fact break your builds if you do so.
6. Place all your code in the `src/` directory. Don't forget to update the version number and the changelog in your spec file!
7. Commit all your changes to your new repo.
8. Treat yourself to a coffee while Gitlab runs your new pipelines! A Koji scratch (ie. temporary) build will be created, allowing you to test if your build succeeds or not. If your build takes more than an hour, you may need to [increase the timeout](#ci-jobs-timing-out) for your Gitlab project.
9. Once you're satisfied with your RPM and the Gitlab CI pipelines are passing, you can [create a tag](https://git-scm.com/book/en/v2/Git-Basics-Tagging) in your git repo. When you create a tag, Gitlab will run the tests again and, if they pass, will then launch a (non-scratch, ie. permanent) build job in Koji. *Protip: use the version of the new RPM as the tag name, future you will thank you. In any case, RPMCI will [add a VCS](#vcs-tag-in-spec-files) tag to your spec file automatically.*
10. When that build succeeds, you'll be able to manually promote it to QA of your Koji tag, and from there to Stable.


### Available Configuration Variables:

Here are the basic configuration options you can set in your `.gitlab-ci.yml` file, and their defaults:

```yaml
  KOJI_TAG: ''
  BUILD_9al: 'False'
  BUILD_9el: 'False'
  BUILD_8al: 'False'
  BUILD_8el: 'False'
  DIST_9al: '.al9.cern'
  DIST_9el: '.rh9.cern'
  DIST_8al: '.al8.cern'
  DIST_8el: '.rh8.cern'
```

If your Koji tag is configured with a custom disttag (such as `.al9.cern`), make
sure you add it to the appropriate `DIST_*` variable.

Your Koji tag will usually be configured in the `KOJI_TAG` variable and it will automatically
get the OS' release appended to it (ie. `KOJI_TAG: config` will become `config8al`, etc.).
If you need to configure a different tag for a particular OS, you can do so using the `KOJI_TAG_*`
variables, but you will have to specify the OS release yourself (ie. `KOJI_TAG_9el: el9`):

```yaml
  KOJI_TAG: 'mytag'
  KOJI_TAG_8al: 'myoldtag8'
  BUILD_9al: 'True'
  BUILD_8al: 'True'
```

## Convenience Functions

In order to simplify common operations, RPMCI includes a series of [convenience functions](functions/)
which are built into the builder (and kojicli) docker images. These functions can
be executed from your gitlab-ci scripts and allow you to easily perform common
operations such as adding your Koji tag as a repo without having to worry about internal
RPMCI variables and details. This also allows you to future-proof your scripts in case
new versions of RPMCI change implementation details.

Here is a partial list of the available functions, please check [functions/](functions/) for the full list:

* `rpmci_add_repo_kojitag_stable`: add your stable Koji tag as a repo. If you add a parameter
  (ie: `rpmci_add_repo_kojitag_stable config8al`), then it adds the stable repo for the specified tag.
* `rpmci_add_repo <name> <url>`: add a custom repo.
* `rpmci_is_OS8el`, `rpmci_is_OS8al`, `rpmci_is_ALMA`, etc: test the OS a particular job is running on.
* `rpmci_install_builddeps`: install the build dependencies specified in the spec file.
* `rpmci_install_requirements`: install the requirements specified in the spec file.
* `rpmci_install_koji_rpms`: install the RPMs built by Koji (for the testing stages).
* `rpmci_enable_module`: enable a dnf module.
* `get_kojitag`: get your full configured Koji tag name in a running job.
* `echo_error`: print a message in bold red.
* `echo_info`: print a message in green.
* `echo_command`: print a message in bold green.

Note that these convenience functions are _only_ available in the RPMCI docker images, `builder-*` and `kojicli`.

## Building for a new OS

If you are already using RPMCI and want to add a build for a new OS, here's
a simple procedure you can follow:

1. Verify if you already have Koji tags for the new OS, or
[request them](https://cern.service-now.com/service-portal?id=sc_cat_item&name=Koji-tag&fe=IT-RPM) if necessary.
2. Add your package to your new tags.
3. Update your `.gitlab-ci.yml` to enable builds (via the `BUILD_*` variables)
for the new OS. Don't forget to adjust the `DIST_*` variables as necessary.
Commit and push [your changes](https://gitlab.cern.ch/linuxsupport/rpms/cern-config-users/-/commit/fca61bc1e082fdd752eabb85832a09cdc53439c8).
1. Now is a good time to stop builds for deprecated OSes. You can remove `BUILD_6`, `BUILD_8`, `BUILD_8s` and `BUILD_9`
(and any other `*_6`, `*_7`, `*_8`, `*_8s` and `*_9` variables) to stop builds for SLC6, CentOS Linux 7, CentOS Linux 8, CentOS Stream 8 and CentOS Stream 9, which may stop working at any time.
1. Now is also a great time to [add some tests](#adding-your-own-tests) to your pipeline!
2. If there haven't been any code changes to your actual RPM since the last release,
you can rebuild it for the new OS:
   1. Find the git tag you created for your last release: `git tag -l -n1`
   2. Create a new tag with the same name but append something to it, like `.alma8`: `git tag v2.45-1.alma8`
   3. Push your new tag to Gitlab.
   4. RPMCI will start the new release process. Since the RPM version hasn't changed, the
   `Koji_build` steps for your previous OSes [will all fail](https://gitlab.cern.ch/linuxsupport/rpms/cern-config-users/-/pipelines/2352412)
   because the build already exists, but you can ignore them.
   5. Once the `Koji_build` step for the new OS succeeds, you can now test and tag
   your build as normal.
   6. You will now have the same version on all OSes without having to artificially bump the release.
1. If you did make code changes to your RPM, you will have to create a new release for all OSes:
   1. Update you spec file to increase the RPM version
   2. git tag the new release, RPMCI will build it as usual.
   3. Continue with the usual procedure.


## Customizations

### Adding your own tests

This CI pipeline includes a `test` stage that is run after building the scratch RPM
on Koji. There's a "base" test job called `.test_install` that installs your RPMs
in the `before_script` section. This job is then extended by OS-specific jobs
(`test_install8el`, `test_install8al`, etc.), which are the ones actually executed.

If you want to run extra actions (functional tests, etc.) once your RPMs are installed,
you can do so in the `script` section:

```yaml
.test_install:
  script:
    - /usr/bin/client --selftest
    - printf "[config]\nserver = https://myserver.cern.ch\n" > /etc/client.conf
    - /usr/bin/client --debug login
```

You can add any necessary dependencies in the `before_script` section, but you will then have to
also include a command to install your RPMs. You can modify the `.test_install` job directly to
affect all OSes, or modify only the OS-specific jobs.

You can also add your own tests by adding another job that extends one of
the OS-specific jobs.

Here are a few examples:

### Adding a dependency for all OSes and tests

```yaml
.test_install:
  before_script:
    - yum install -y --nogpgcheck --enablerepo=cernonly oracle-instantclient-tnsnames.ora
    - rpmci_install_koji_rpms
```

### Adding your own Koji tag's repo (for extra dependencies)

This makes use of the `rpmci_add_repo_kojitag_stable` convenience function, which
will add your Koji tag's stable repo. You will still need to install your Koji-built
RPMs, and for that you can use the `rpmci_install_koji_rpms` function.

```yaml
.test_install:
  before_script:
    - rpmci_add_repo_kojitag_stable
    - rpmci_install_koji_rpms
```

### New job with extra tests

```yaml
test_nose8al:
  extends: test_install8al
  script:
    # These deps are only needed to run the unit tests
    - yum install --nogpgcheck -y python-nose python2-mock python-suds
    - cd t
    - nosetests

test_nose9al:
  extends: test_install9al
  before_script:
    - yum install -y python2-future
    - rpmci_install_koji_rpms
  script:
    # These deps are only needed to run the unit tests
    - yum install --nogpgcheck -y python-nose python2-mock python-suds
    - cd t
    - nosetests
```

### New job running pylint

```yaml
pylint:
  stage: prebuild
  script:
    # Install all the depedencies defined in the spec file
    - rpmci_install_requirements
    - yum install -y pylint
    - pylint mysourcecode/
```

### Replacing a particular OS' test

You may not want to install all the RPMs that you've built:

```yaml
test_install9al:
  before_script:
    - yum install -y --nogpgcheck koji/myrpm-client*.rpm
```

### Configuring rpmlint

You can create a custom `.rpmlint` configuration file to disable some tests:

```
addFilter("E: hardcoded-library-path")
addFilter("myrpm.spec:6: W: non-standard-group")
```

### Ignoring rpmlint

If you don't care about rpmlint results, you can turn it into a manual job
so it's not run automatically:

```yaml
.test_rpmlint:
  when: manual
```

You can also let it run, but not have it stop the pipeline if there are errors:

```yaml
.test_rpmlint:
  allow_failure: true
```

### Adding build dependencies

If your local builds (`srpm` and `rpm` stages) require dependencies from other repos,
such as from your Koji tag, you can add them for all OSes as follows:

```yaml
.rpm_deps:
  before_script:
    - rpmci_add_repo_kojitag_stable
    - rpmci_install_builddeps
```

You can also add different dependencies per OS, like this:

```yaml
.rpm_deps9al:
  before_script:
    - rpmci_add_repo_kojitag_stable "dbclients9al"
    - yum install -y oracle-instantclient
    - rpmci_install_builddeps

.rpm_deps8al:
  before_script:
    - rpmci_add_repo_kojitag_stable "dbclients8al"
    - yum install -y oracle-instantclient
    - rpmci_install_builddeps
```

You may also perform conditional actions depending on the OS by using the `rpmci_is_*` convenience functions:

```yaml
.rpm_deps:
  before_script:
    - |
      if rpmci_is_OS9al; then
        rpmci_add_repo_kojitag "dbclients9al-stable"
      fi
    - rpmci_install_builddeps
```

You can add arbitrary repositories using `rpmci_add_repo`. If you need to change a repo's options,
such as disabling the gpg check, you can do so with `yum-config-manager`:

```yaml
.rpm_deps9al:
  before_script:
    - rpmci_add_repo myrepo "http://remote.com/myrepo/"
    - yum-config-manager --setopt "myrepo.gpgcheck=0" --save myrepo
    - rpmci_install_builddeps
```

Don't forget to include the `rpmci_install_builddeps` line in your `before_script`, otherwise
no dependencies will actually be installed!

### Adding your own CI stages

There are some unused stages (`prebuild`, `prekoji`, `pretest`, `posttest`, `postkoji`,
`docker_prepare`, `docker_build`, `docker_test`, `postqa` and `postdeploy`) which you can use to define your own custom jobs.

If these are insufficient, or you wish to add/modify/reorder the stages of the CI process,
you will have to redefine the ones included in [rpm-ci.yml](https://gitlab.cern.ch/linuxsupport/rpmci/blob/master/rpm-ci.yml)
and add your own in your own `.gitlab-ci.yml` file. Note, however, that there are
`dependencies` and `needs` clauses between some jobs that you may need to modify.

### Skipping the Gitlab RPM build step

RPMCI builds the source and the binary RPM, but only the source RPM is really needed for
building in Koji later. The binary RPM is built in Gitlab as a sanity check and to prevent
broken RPMs from being submitted to Koji. There are situations (such as upstream packages
that take a really long time to build) where this step is overkill, so it can be disabled:

```yaml
.build_rpm:
  when: manual
```

The Gitlab pipelines will still contain jobs in the `rpm` stage and you can run them
manually if you want, but they will not be run automatically and they won't delay the
rest of the pipeline.

### Skipping the download of Koji build artifacts

If your packages are very large and you don't want them added to the Gitlab pipeline,
you can tell RPMCI to not download them from Koji:

```yaml
variables:
  SKIP_KOJI_DOWNLOAD: 'True'
```

You will always be able to download them directly from Koji and you'll have direct links
to do so as part of the "Koji build summary" at the end of the job.

Note that disabling Koji downloads will also disable the check to see if the DIST tag is correct
and the testing stages (as there are no packages available to test).

### Using rpmautospec

If you'd like to use [rpmautospec](https://docs.pagure.org/fedora-infra.rpmautospec/) to autogenerate your
changelog and release based on your git history, you can enable preprocessing of your spec file:

```
variables:
  RPM_AUTOSPEC: 'True'
```

Your spec file must be pre-converted first using `rpmautospec convert mypkg.spec`. This will move
your current changelog to another file, it will add the `%autorelease` and `%autochangelog` macros
to your spec file and it will commit the changes.

Once that's done, subsequent commits will increment the release and add your commit message to the
changelog. If you don't want a commit to add a changelog entry, include `[skip changelog]` in your
commit message. For more information, please consult the [upstream documentation](https://docs.pagure.org/fedora-infra.rpmautospec/).
Setting your gitlab merge request settings to "Fast-forward merge" is advised to avoid merge commits and them creating a phantom release increment.

Note that `rpmautospec` is only available starting from RHEL/AlmaLinux 8/9, it's not supported on
any previous OS (including CentOS Linux 8, CentOS Stream 8 or CentOS Stream 9).

## Tips and Tricks

### Error: No source RPMs found

If you get this error, it probably means your Makefile does not conform to what
RPMCI requires or expects. The [RPM starter](https://gitlab.cern.ch/linuxsupport/myrpm)
repo provides a good [Makefile](https://gitlab.cern.ch/linuxsupport/myrpm/-/blob/master/Makefile) to follow.

In particular, make sure the [DIST](https://gitlab.cern.ch/linuxsupport/myrpm/-/blob/master/Makefile#L6) variable
is conditionally assigned (it will normally be assigned by RPMCI) and that this
variable is [passed to rpmbuild](https://gitlab.cern.ch/linuxsupport/myrpm/-/blob/master/Makefile#L18).
Make sure that `_topdir` is also set correctly.

### Debugging builds locally

The best way to debug your builds locally is to run them within the same Docker images RPMCI uses. You can do something like this:

```bash
~ > git clone https://:@gitlab.cern.ch:8443/linuxsupport/myrpm.git
...
~ > docker run --rm -it -v ~/myrpm:/build:z gitlab-registry.cern.ch/linuxsupport/rpmci/builder-cs8:latest
...
[root@99369f9e7a22 /]# cd /build/
[root@99369f9e7a22 build]# yum-builddep -y *.spec
...
[root@99369f9e7a22 build]# make rpm
...
```

If it builds here, it will work in Gitlab CI and it has a high chance of success in Koji.

### CI jobs timing out

Gitlab CI's default timeout for jobs is 1 hour. If one of your jobs takes longer than that,
it will be terminated and the pipeline won't continue. You can change this timeout for your
project in `Settings` -> `CI / CD` -> `General pipelines` -> `Timeout`.

### Dynamic Build Requires

rpmci supports [dynamic build requires](https://fedoraproject.org/wiki/Changes/DynamicBuildRequires)
where available (currently only CS9). You can add a `%generate_buildrequires` section
to your spec file and let your language tooling automatically generate your build dependencies.
Check the documentation linked above for more information.

### Building Go binaries on CC7

If you're building golang binaries on CC7 and you want the EPEL golang, you'll have to do the following:

```yaml
build_srpm7:
  before_script:
    - yum-builddep -y --disableplugin=protectbase,priorities *.spec

build_rpm7:
  before_script:
    - yum-builddep -y --disableplugin=protectbase,priorities *.spec
```

Your spec file should also contain a `BuildRequires: golang`.

Lastly, the `git` version available on CC7 can produce errors like the following while fetching Go dependencies:

```
fatal: git fetch-pack: expected shallow list
```

One solution to this issue is to use `rh-git29` software collection. To do that, you should define a `before_script` similar to the following one:

```
  before_script:
    (...)
    - yum-config-manager --add-repo "http://linuxsoft.cern.ch/cern/centos/7/sclo/x86_64/rh"
    - yum install -y --nogpgcheck rh-git29
    - export PATH=/opt/rh/rh-git29/root/usr/bin:$PATH
    - export LD_LIBRARY_PATH=/opt/rh/httpd24/root/usr/lib64:$LD_LIBRARY_PATH
    - export MANPATH=/opt/rh/rh-git29/root/usr/share/man:$MANPATH
    - export PERL5LIB=/opt/rh/rh-git29/root/usr/share/perl5/vendor_perl
    - rpmci_install_builddeps
```

This could be simplified by calling `source /bin/scl_source enable rh-git29`, but unfortunately it seems to fail.

### Building Go packages that have external dependencies

Fetching external dependencies will fail in Koji builds as the builders don't have internet access. Instead, you should bundle all dependencies in the srpm.

You can do that easily with the `go mod vendor` command which creates a `vendor` directory with a copy of all dependencies. You can choose to include these with the project or only download them when creating the srpm.

To do the second, you simply need to run the command when creating the .tgz archive in your `Makefile`. For example:

```
sources: vendor
  go mod vendor
	@tar cvzf $(TARFILE) --exclude-vcs --transform 's,^,$(PKGNAME)/,' src vendor
```

Then, when `go build` is executed, it will find and use the vendored versions of the dependencies.

You can also add the following to `.gitlab-ci.yml` to download the dependencies once rather than in each stage.

```yaml
.rpm_deps:
  before_script:
    - rpmci_install_builddeps
    - go mod vendor
  cache:
    paths:
      - vendor
```

### Building Maven packages

Normal Maven builds will not work in Koji because Koji builders don't
have internet access. The solution to this is to cache all the dependencies
before sending the job to Koji.

Edit your `Makefile` to download all the dependencies to the `.m2` directory and add it to the package:

```Makefile
sources:
    mvn -Dmaven.repo.local="$(PWD)/.m2" surefire:help dependency:go-offline
    tar -zcvf $(TARFILE) --transform 's,^,$(SPECFILE_NAME)-$(SPECFILE_VERSION)/,' src .m2 pom.xml
```

Edit your spec file to add `maven-local` and build from `.m2`:

```
...
BuildRequires:  maven-local
...

%setup -q
%mvn_file : %{name}/%{name} %{name}

%build
%mvn_build -f -j -- -Dmaven.repo.local=".m2"

%install
%mvn_install
...
```

You can find a full example of a Maven build [here](https://gitlab.cern.ch/ComputerSecurity/cert-flume-extra/-/tree/master).

### VCS tag in Spec files

RPMCI will automatically add a `VCS` tag to your spec file that
points to your gitlab repo at the commit hash the RPM is built from. This gives
you another way of tracing the source code that was used to build your package.

You can see the value of this tag on a built package like this:

```bash
~ > rpm -qp --qf "%{VCS}\n" ~/myrpm-1.1-6.el8.x86_64.rpm
https://gitlab.cern.ch/linuxsupport/myrpm/-/tree/60f909134d2ec5895cec23eabc75084502d091d7
```

You will also find the `VCS` tag in Koji by looking at a information page for a [build](https://koji.cern.ch/buildinfo?buildID=47852)
or an [RPM](https://koji.cern.ch/rpminfo?rpmID=287030).

By default, the `VCS` tag is `"${CI_PROJECT_URL}/-/tree/${CI_COMMIT_SHA}"`, but you can
customize this by supplying your own format in the `_VCS_TAG` variable.

The `VCS` tag won't be added if one already exists in the spec file. You can also
stop adding this tag altogether by setting the variable `ADD_VCS_TAG` to `False`.

## Getting Help

If you run into issues on how to use RPMCI, you can contact us on the [Linux Mattermost channel](https://mattermost.web.cern.ch/it-dep/channels/linux)
or by opening a [ServiceNOW ticket](https://cern.service-now.com/service-portal?id=sc_cat_item&name=incident&fe=IT-RPM).
